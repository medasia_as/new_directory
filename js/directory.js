$(document).ready(function(){
  type = 'hospital';
  div  = ''
  var table = "hospital";
  var global_search = "";
  var global_type = "hospital";
  var global_city = "";
  var global_region = "";
  var global_province = "";
  var global_status = 'old';
  $("#search_by_dentist_doctors").hide();
  $("#search_clinic_by_address").hide();
  $('#search_new_dentistdoctors_address').hide();
  specialization = "";
  search_clinic = "";

  String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
  }

  search_template = function(data){
    // console.log(data);
    template = '<span style="color:grey; display: block;">' + data.title + '</span>' +
                   '<select name="search_by_' + data.id + '" class="dropdown" id="search_by_' + data.id + '">' +
                   template_option(data.options, data.id) +
                   '</select>';
    return template;
  };

  template_option = function(options, id) {
    template = '<option value=""></option>';
    options = $.parseJSON(options);
    $.each(options, function(key, value) {
    if (value[id] != "") {
      template += '<option value="'+value.value+'">'+value.label+'</option>';
      }
    });
    return template;
  }

  search_result_template = function(data){
    // console.log(data);
    if(data.count == 0)
    {
      $('#results').html('<p><center>No Results Found</b></center></p>');
    }
    else
    {
      if(table == "hospital")
      {
       template = search_result_hospiclinic_template(data.hospital);
      }
      else if(table == "dentistsanddoctors")
      {
        if(global_status == "old")
        {
          template = search_result_dentistdoctors_old_template(data.dentistsanddoctors);
        }
        else if(global_status == "new")
        {
          template = search_result_dentistdoctors_new_template(data.dentistsanddoctors);
        }
      }
      else if(table == 'establishments')
      {
        template = search_result_dentistdoctors_new_template(data.dentistsanddoctors);
      }
      result_template = '<p><center>Number of '+ data.search +' : <b>'+ data.count +'</b></center></p>' +

                      '<center><table border="0" cellpadding="2" width="680px" id="table_results">' +
                      template +
                      '</table></center>';
      $('#results').html(result_template);
    }
  };

  search_result_hospiclinic_template = function(results){
    result_template = "";
    $.each(results, function(key,value) {
      result_template += '<tr><th colspan="2" align="left">'+value.name+' - '+value.branch+' Branch</th></tr>' +
                          '<tr><td width="30%" align="right">Address :</td><td>'
                            +value.street_address+' '
                            +value.subdivision_village+' '
                            +value.barangay+' '
                            +value.city+' '
                            +value.province+' '
                            +value.region+' '
                            +'</td></tr>';
                          if(value.contact_person)
                          {
                            result_template += '<tr><td align="right">Contact Person :</td><td>'+value.contact_person+'</td></tr>';
                          }
                          if(value.contact_number)
                          {
                            result_template += '<tr><td align="right">Contact Number :</td><td>'+value.contact_number+'</td></tr>';
                          }
                          if(value.type)
                          {
                            result_template += '<tr><td align="right">Type :</td><td>'+value.type+'</td></tr>';
                          }
                          if(value.category)
                          {
                            result_template += '<tr><td align="right">Category :</td><td>'+value.category+'</td></tr>';
                          }
      result_template += '<tr><td>&nbsp</td></tr>';
    });
    return result_template;
  };

  search_result_dentistdoctors_old_template = function(results){
    result_template = "";
    $.each(results, function(key,value) {
        result_template += "<tr><th colspan='2' align='left'>DR. "+value.firstname+" "+value.middlename+" "+value.lastname+"</th></tr>";
                      if(value.specialization != "-")
                      {
                        result_template += "<tr><td width='30%' align='right'>Specilization :</td><td>"+value.specialization+"</td></tr>";
                      }
                      if(value.contact_number != "-")
                      {
                        result_template += "<tr><td width='30%' align='right'>Contact Number :</td><td>"+value.contact_number+"</td></tr>";
                      }
                      if(value.mobile_number != "-")
                      {
                        result_template += "<tr><td width='30%' align='right'>Mobile Number :</td><td>"+value.mobile_number+"</td></tr>";
                      }
                      if(value.fax_number != "-")
                      {
                        result_template += "<tr><td width='30%' align='right'>Fax Number :</td><td>"+value.fax_number+"</td></tr>";
                      }
                      if(value.email != "-")
                      {
                        result_template += "<tr><td width='30%' align='right'>E-mail :</td><td>"+value.email+"</td></tr>";
                      }
                      if(value.clinic1 != "-")
                      {
                        result_template += "<tr><td width='30%' align='right'>Clinic 1 :</td><td>"+value.clinic1+"</td></tr>";
                      }
                      if(value.clinic2 != "-")
                      {
                        result_template += "<tr><td width='30%' align='right'>Clinic 2 :</td><td>"+value.clinic2+"</td></tr>";
                      }
                      if(value.clinic3 != "-")
                      {
                        result_template += "<tr><td width='30%' align='right'>Clinic 3 :</td><td>"+value.clinic3+"</td></tr>";
                      }
                      if(value.clinic4 != "-")
                      {
                        result_template += "<tr><td width='30%' align='right'>Clinic 4 :</td><td>"+value.clinic4+"</td></tr>";
                      }
                      if(value.clinic5 != "-")
                      {
                        result_template += "<tr><td width='30%' align='right'>Clinic 5 :</td><td>"+value.clinic5+"</td></tr>";
                      }
        result_template += "<tr><td>&nbsp</td></tr>";
    });
  return result_template;
  };

  search_result_dentistdoctors_new_template = function(results){
    result_template = "";
    $.each(results, function(key,value) {
        result_template += "<tr><th colspan='2' align='left'>DR. "+value.firstname+" "+value.middlename+" "+value.lastname+"</th></tr>";
                      if(value.specialization != "-")
                      {
                        result_template += "<tr><td width='30%' align='right'>Specilization :</td><td>"+value.specialization+"</td></tr>";
                      }
                      if(value.contact_number != "-")
                      {
                        result_template += "<tr><td width='30%' align='right'>Contact Number :</td><td>"+value.contact_number+"</td></tr>";
                      }
                      if(value.mobile_number != "-")
                      {
                        result_template += "<tr><td width='30%' align='right'>Mobile Number :</td><td>"+value.mobile_number+"</td></tr>";
                      }
                      if(value.fax_number != "-")
                      {
                        result_template += "<tr><td width='30%' align='right'>Fax Number :</td><td>"+value.fax_number+"</td></tr>";
                      }
                      if(value.email != "-")
                      {
                        result_template += "<tr><td width='30%' align='right'>E-mail :</td><td>"+value.email+"</td></tr>";
                      }
                      console.log(Array.isArray(value.address));
                      if(Array.isArray(value.address))
                      {
                        $.each(value.address, function(key2,value2){
                          result_template += "<tr><td width='30%' align='right'>Clinic "+(key2+1)+":</td><td>"+value2+"</td></tr>";
                        });
                      }
        result_template += "<tr><td>&nbsp</td></tr>";
    });
  return result_template;
  };

  ajax_search = function(variables){
    // console.log(variables);
    $.ajax({
      type: 'POST',
      data: variables,
      url:  'index.php/directories/ajaxSearch/',
      dataType: 'json',
      success: function(data) {
        search_result_template(data); // JSON FORMAT
        // $('#results').html(data); // HTML FORMAT
      }
    });
  };

  ajax_search_sub_field = function(variables){
    // console.log(variables);
    $.ajax({
      type: 'POST',
      // data: {select: 'distinct province', from: 'hospital',  where: 'region = \'' + region + '\'', order: 'province'},
      data : variables,
      url:  'index.php/directories/searchFieldSub/',
      success: function(data) {
        if(variables.sub == 'city')
        {
          title = 'Search City / Town ';
        }
        else
        {
          title = 'Search '+variables.sub.capitalize();
        }
        if(variables.table == 'establishments')
        {
          $('#search_by_result_new').append(search_template({title: title, options: data, id: variables.sub}));
        }
        else
        {
          $('#search_by_result').append(search_template({title: title, options: data, id: variables.sub}));
        }
      }
    });
  };

  $(document).on('change', 'select[name="type"]', function(e){
    type = $(this).val();
    $('input[name="search"]').attr("placeholder", "Search "+type.capitalize()).val("");
    global_type = type;
    global_city = "";
    global_region = "";
    global_province = "";
    specialization = "";
    // global_status = "";
    $('#search_new_dentistdoctors_address').hide();

    $('#search_by_region').prev().remove();
    $("#search_by_province").prev().remove();
    $("#search_by_city").prev().remove();
    $('#search_by_region').remove();
    $("#search_by_province").remove();
    $("#search_by_city").remove();

    switch(type)
    {
      case 'hospital':
      case 'clinic':
        table = "hospital";
        $('#search_label').text('Search By');
        $("#search_by_hospital_clinic").show();
        $("#search_by_dentist_doctors").hide();
        $("#search_clinic_by_address").hide();
        ajax_search({'table': table, 'type': global_type, 'region': global_region, 'province': global_province, 'city': global_city, 'search':global_search});
        $("#searchby").val('');
        $(".search_by").removeClass('show');
        $(".search_by_new").removeClass('show');
      break;

      case 'dentist':
      case 'doctor':
        global_status = 'old';
        $('#searchby_dentistanddoctor').val('old');
        if(type == 'dentist')
        {
          global_type = 'DMD';
        }
        else if(type == 'doctor')
        {
          global_type = 'MD';
        }

        $('#search_label').text('Choose Directory to Search from');
        $("#search_by_hospital_clinic").hide();
        $("#search_by_dentist_doctors").show();

        $('#search_by_region').prev().remove();
        $("#search_by_province").prev().remove();
        $("#search_by_city").prev().remove();
        $('#search_by_region').remove();
        $("#search_by_province").remove();
        $("#search_by_city").remove();

        $('#search_by_dentist').prev().remove();
        $('#search_by_doctor').prev().remove();
        $('#search_by_dentist').remove();
        $('#search_by_doctor').remove();

        $('#search_by_dentistsanddoctors').prev().remove();
        $('#search_by_dentistsanddoctors').remove();
        $('#search_by_establishments').prev().remove();
        $('#search_by_establishments').remove();

        $(".search_by").addClass("show");

        if(global_status == 'old')
        {
          table = "dentistsanddoctors";
          $("#search_clinic_by_address").show();
        }
        else if(global_status == 'new')
        {
          table = "establishments";
          $("#search_clinic_by_address").hide();
        }


        ajax_search({'table': table, 'type': global_type, 'region': global_region, 'province': global_province, 'city': global_city, 'search':global_search, 'directory': global_status,'specialization': specialization, 'clinic' : search_clinic});
        $.ajax({
          type: 'POST',
          // data: {select: 'distinct ' + div, from: 'hospital',  where: '', order: div},
          data: {'table':table,'field':'specialization', type: { classification: global_type, doctor_status: global_status }},
          url:  'index.php/directories/searchField/',
          success: function(data) {
            // console.log(data);
            title = 'Search Specialization';
            $('#search_by_result').append(search_template({title: title, options: data, id: table}));
            // $('#search_by_result').append('<span style="color:grey; display: block;">Search Address(Region/Province/City)</span>'+
            //           '<input type="text" name="search_by_clinic" id="search_by_clinic" style="width:292px; display: block" class="dropdown">');
          }
        });
      break;
    }
  });

  $(document).on('change', '#searchby', function(e){
    div = $(this).val();
    global_city = "";
    global_region = "";
    global_province = "";
    $('#search_by_result').html('');

    console.log("test");
    if (div != "") {
      $(".search_by").addClass("show");
      // console.log(div, type);
      console.log({'table':table,'field':div,'type':global_type});
      $.ajax({
        type: 'POST',
        // data: {select: 'distinct ' + div, from: 'hospital',  where: '', order: div},
        data: {'table':table,'field':div,'type':global_type, status: global_status},
        url:  'index.php/directories/searchField/',
        success: function(data) {
          // console.log(data);
          title = 'Search ';
          if (div == 'city')
            title += 'City / Town '
          else
            title += div.capitalize()
          $('#search_by_result').append(search_template({title: title, options: data, id: div}));
        }
      });
    } else {
      $('#search_by_result').html('');
      $(".search_by").removeClass("show");
    }
  });

  $('#search_button').click(function(){
    ajax_search({'table': table, 'type': global_type, 'region': global_region, 'province': global_province, 'city': global_city, 'search':global_search, 'directory': global_status,'specialization': specialization, 'clinic' : search_clinic});
  });

  $('input[name="search"]').keyup(function() {
    global_search = $(this).val();
    ajax_search({'table': table, 'type': global_type, 'region': global_region, 'province': global_province, 'city': global_city, 'search':global_search, 'directory': global_status,'specialization': specialization, 'clinic' : search_clinic});
  });

  $('input[name="search_by_clinic"]').keyup(function() {
    search_clinic = $(this).val();
    ajax_search({'table': table, 'type': global_type, 'region': global_region, 'province': global_province, 'city': global_city, 'search':global_search, 'directory': global_status,'specialization': specialization, 'clinic' : search_clinic});
  });

  $(document).on('change', "#search_by_region", function(){
    region = $(this).val();
    global_region = region;
    global_city = "";
    global_province = "";

    $("#search_by_province").prev().remove();
    $("#search_by_city").prev().remove();
    $("#search_by_province").remove();
    $("#search_by_city").remove();

    ajax_search_sub_field({'table':table,'field':'region','value':region,'sub':'province'});
    ajax_search({'table': table, 'type': global_type, 'region': global_region, 'province': global_province, 'city': global_city, 'search':global_search});
  });

  $(document).on('change', "#search_by_province", function(){
    province = $(this).val();
    global_province = province;
    global_city = "";

    $("#search_by_city").prev().remove();
    $("#search_by_city").remove();

    ajax_search_sub_field({'table':table,'field':'province','value':province,'sub':'city'});
    ajax_search({'table': table, 'type': global_type, 'region': global_region, 'province': global_province, 'city': global_city, 'search':global_search, 'directory': global_status,'specialization':specialization});
  });

  $(document).on('change', "#search_by_city", function(){
    city = $(this).val();
    global_city = city;

    ajax_search({'table': table, 'type': global_type, 'region': global_region, 'province': global_province, 'city': global_city, 'search':global_search,'directory': global_status,'specialization':specialization});
  });

  $(document).on('change', '#searchby_dentistanddoctor', function(){
    table = "dentistsanddoctors";
    global_status = $(this).val();
    search_clinic = "";
    specialization = "";
    $('#search_by_region').prev().remove();
    $("#search_by_province").prev().remove();
    $("#search_by_city").prev().remove();
    $('#search_by_region').remove();
    $("#search_by_province").remove();
    $("#search_by_city").remove();
    $('#search_by_dentistsanddoctors').prev().remove();
    $('#search_by_dentistsanddoctors').remove();
    $('#search_by_establishments').prev().remove();
    $('#search_by_establishments').remove();

    if(global_status == 'old')
    {
      table = 'dentistsanddoctors';
      $("#search_clinic_by_address").show();
      $('#search_new_dentistdoctors_address').hide();
      // $("#search_by_hospital_clinic").hide();
    }
    else if(global_status == 'new')
    {
      table = 'establishments';
      $("#search_clinic_by_address").hide();
      // $("#search_by_hospital_clinic").show();
    }

    // console.log({'table':table,'field':'specialization', type: { classification: global_type, doctor_status: global_status }});
    // ajax_search({'table': table, 'type': global_type, 'region': global_region, 'province': global_province, 'city': global_city, 'search':global_search, 'directory': global_status,'specialization': specialization, 'clinic' : search_clinic});
    ajax_search({'table': table, 'type': global_type, 'region': global_region, 'province': global_province, 'city': global_city, 'search':global_search, 'directory': global_status,'specialization':specialization});
    $.ajax({
      type: 'POST',
      // data: {select: 'distinct ' + div, from: 'hospital',  where: '', order: div},
      data: {'table':'dentistsanddoctors','field':'specialization', type: { classification: global_type, doctor_status: global_status }},
      url:  'index.php/directories/searchField/',
      success: function(data) {
        console.log(data);
        title = 'Search Specialization';
        $('#search_by_result').append(search_template({title: title, options: data, id: table}));
      }
    });
  });

  $(document).on('change', '#search_by_dentistsanddoctors', function(){
    // $('#search_by_region').prev().remove();
    $("#search_by_province").prev().remove();
    $("#search_by_city").prev().remove();
    // $('#search_by_region').remove();
    $("#search_by_province").remove();
    $("#search_by_city").remove();
    specialization = $(this).val();
    // console.log(specialization);
    ajax_search({'table': table, 'type': global_type, 'region': global_region, 'province': global_province, 'city': global_city, 'search':global_search, 'directory': global_status,'specialization': specialization, 'clinic' : search_clinic});

    if(global_status == 'new')
    {
      $('#search_new_dentistdoctors_address').show();
    }
    if(specialization == '')
    {
      $('#search_new_dentistdoctors_address').hide();
      $('#searchby_new_dentistdoctors').val('');
    }
  });

  $(document).on('change', '#search_by_establishments', function(){
    // $('#search_by_region').prev().remove();
    $("#search_by_province").prev().remove();
    $("#search_by_city").prev().remove();
    // $('#search_by_region').remove();
    $("#search_by_province").remove();
    $("#search_by_city").remove();
    specialization = $(this).val();
    // console.log(specialization);
    ajax_search({'table': table, 'type': global_type, 'region': global_region, 'province': global_province, 'city': global_city, 'search':global_search, 'directory': global_status,'specialization': specialization, 'clinic' : search_clinic});

    if(global_status == 'new')
    {
      $('#search_new_dentistdoctors_address').show();
    }
    if(specialization == '')
    {
      $('#search_new_dentistdoctors_address').hide();
      $('#searchby_new_dentistdoctors').val('');
    }
  });

  $(document).on('change', '#searchby_new_dentistdoctors', function(e){
    div = $(this).val();
    table = 'establishments';
    global_city = "";
    global_region = "";
    global_province = "";
    // $('#search_by_region').prev().remove();
    $("#search_by_province").prev().remove();
    $("#search_by_city").prev().remove();
    // $('#search_by_region').remove();
    $("#search_by_province").remove();
    $("#search_by_city").remove();

      $(".search_by_new").addClass("show");
      // console.log(div, type);

      $.ajax({
        type: 'POST',
        // data: {select: 'distinct ' + div, from: 'hospital',  where: '', order: div},
        data: {'table': table,'field':div,'type':global_type, 'status': global_status},
        url:  'index.php/directories/searchField/',
        success: function(data) {
          // console.log(data);
          title = 'Search ';
          if (div == 'city')
            title += 'City / Town '
          else
            title += div.capitalize()
          $('#search_by_result_new').append(search_template({title: title, options: data, id: div}));
        }
      });
  });
});