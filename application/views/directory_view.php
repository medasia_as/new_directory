<!DOCTYPE html>
<html>
<head>
  <title>Medriks Directory</title>
  <link rel="stylesheet" type="text/css" href="style/main_style.css">

  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
  <script src="js/directory.js" type="text/javascript"></script>
</head>

<body>
<div>
  <div id="banner-form">
    <div border='0'>
        <div><img src='image/medasia.png' border='0' class="headerimg" ></img></div>
        <div><strong>Medical Network Directory</strong></div>
    </div>
  <!-- <p class="pageTitle2">&nbsp;Directory</p> -->

  <form id='searchForm' name='searchForm' method="post" >
          <div border="0">
            <div>
              <div id="div_search">
                <input type="text" name="search"  class="dropdown" placeholder="Search Hospital"/>
              </div>
            </div>

            <div>
              <div  style="color:grey;"> Select </div>
            </div>

            <div>
              <div>
                <select name="type" id="type" class="dropdown" >
                  <option value="hospital">Hospital</option>
                  <option value="clinic">Clinic</option>
                  <option value="dentist">Dentist</option>
                  <option value="doctor">Doctor</option>
                </select>
              </div>
            </div>


            <!-- <div>
              <div><span style="color:grey;" id="search_label">Search By</span></div>
            </div> -->

            <!-- search by Dentist and Doctors -->
            <div id="search_by_dentist_doctors">
                <div>
                  <div id="alert"><p>If you cannot find the Doctor that you're looking for,
                    please change <span style="color: red">DIRECTORY</span> to <span style="color:red">NEW</span>.</p></div>
                  <div style="color:grey; display: block;">Choose Directory to Search from</div>
                  <select name="searchby" id="searchby_dentistanddoctor" class="dropdown" >
                    <option value="old"> Old Directory </option>
                    <option value="new"> New Directory </option>
                  </select>
                </div>
              </div>
            <!-- /search by City / Town -->

            <!-- search by clinic/hospital -->
            <div id="search_by_hospital_clinic">
              <div>
                <div style="color:grey; display: block;">Search By</div>
                <select name="searchby" id="searchby" class="dropdown" >
                  <option value=""></option>
                  <option value="city"> City </option>
                  <option value="region"> Region </option>
                  <option value="province"> Province </option>
                </select>
              </div>
            </div>
            <!-- /search by clinic/hospital -->

            <!-- search by City / Town -->
            <div class="search_by">
              <div id="search_by_result">
                <!-- <span style="color:grey; display: block;">Search City / Town</span>
                <select name="searchby" id="searchby" class="dropdown" style="width:300px; display: block">
                  <option value=""></option>
                  <option value="city"> City </option>
                  <option value="region"> Region </option>
                  <option value="province"> Province </option>
                </select> -->
              </div>
            </div>
            <!-- /search by City / Town -->
            <!-- Search by City / Town (Old Directory) -->
            <div>
              <div>
                <div id="search_clinic_by_address">
                  <span style="color:grey; display: block;">Search Address(Region/Province/City)</span>
                  <input type="text" name="search_by_clinic" id="search_by_clinic"  class="dropdown">
                </div>
              </div>
            </div>
            <!-- END -->

            <div>
              <div>
                <div id="search_new_dentistdoctors_address">
                <span style="color:grey; display: block;">Search By</span>
                <select name="searchby_new_dentistdoctors" id="searchby_new_dentistdoctors" class="dropdown" >
                  <option value=""></option>
                  <option value="city"> City </option>
                  <!-- <option value="region"> Region </option> -->
                  <option value="province"> Province </option>
                </select>
                </div>
              </div>
            </div>

            <!-- search by City / Town -->
            <div class="search_by_new">
              <div id="search_by_result_new">
                <!-- <span style="color:grey; display: block;">Search City / Town</span>
                <select name="searchby" id="searchby" class="dropdown" style="width:300px; display: block">
                  <option value=""></option>
                  <option value="city"> City </option>
                  <option value="region"> Region </option>
                  <option value="province"> Province </option>
                </select> -->
              </div>
            </div>

            <div>
              <div colspan=2>
                <input type="hidden" value="View" name="Handler" />
              </div>
            </div>
            <div>
              <div>
                <input type="button" value="Reset" onclick="location.reload();" class="reset_button_new">
                <input type="button" value="Search" id="search_button" class="search_button_new"/></div>
            </div>
          </div>

        </form>
</div>

<div id="div-results">
      <div id="results"></div>
</div>

<a href="#banner-form">
  <img src="<?php echo base_url();?>image/back_to_top_circle.png" id="backToTopButton">
</a>

  </body>
</html>