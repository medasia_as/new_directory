<?php
class Directory_model extends CI_Model
{
  function getAllRecords($table)
  {
    $query = $this->db->get($table);

    if($query->num_rows())
    {
      return $query->result_array();
    }
    else
    {
      return false;
    }
  }

  function getFieldResult($table,$field,$type)
  {
    $query = $this->db->distinct();
    $query = $this->db->select($field);
    $query = $this->db->order_by($field);
    if($table == 'hospital')
    {
      $query = $this->db->where('classification',$type);
    }
    else if($table == 'dentistsanddoctors')
    {
      $query = $this->db->where('type',$type['classification']);
      $query = $this->db->where('doctor_status',$type['doctor_status']);
    }
    $query = $this->db->get($table);

    if($query->num_rows())
    {
      return $query->result_array();
    }
    else
    {
      return false;
    }
  }

  function getFieldSubResult($table,$field,$value,$sub)
  {
    $query = $this->db->distinct();
    $query = $this->db->select($sub);
    $query = $this->db->where($field,$value);
    $query = $this->db->order_by($sub);
    $query = $this->db->get($table);

    if($query->num_rows())
    {
      return $query->result_array();
    }
    else
    {
      return false;
    }
  }

  function getSearchResults($data,$select = '')
  {
    $table = $data['table'];

    if($table == 'hospital')
    {
      // echo 'hello';
      $query = $this->db->select($select);
      $classification = $data['type'];
      $region = $data['region'];
      $province = $data['province'];
      $city = $data['city'];
      $search = $data['search'];
      $query = $this->db->where('classification',$classification);

      if(!empty($region))
      {
        // echo 'region';
        $query = $this->db->where('region',$region);
      }
      if(!empty($province))
      {
        // echo 'province';
        $query = $this->db->where('province',$province);
      }
      if(!empty($city))
      {
        // echo 'city';
        $query = $this->db->where('city',$city);
      }
      if(!empty($search))
      {
        // echo 'name';
        $query->like('name',$search);
      }
      $query = $this->db->get($table);
    }
    else if($table == 'dentistsanddoctors')
    {
      // var_dump($data);
      $query = $this->db->select($select);
      $specialization = $data['specialization'];
      $search = $data['search'];
      $doctor_status = $data['directory'];
      $type = $data['type'];


      $query = $this->db->where('doctor_status', $doctor_status);
      $query = $this->db->where('type', $type);

      if(!empty($specialization))
      {
        $query = $this->db->where('specialization',$specialization);
      }
      if(!empty($search))
      {
        // echo 'hello';
        // $query->like("firstname", $search);
        // $query->or_like("middlename", $search);
        // $query->or_like("lastname", $search);
        $query = $this->db->where("(`firstname` LIKE '%$search%' OR `middlename` LIKE '%$search%' OR `lastname` LIKE '%$search%')");
      }
      if(!empty($data['clinic']))
      {
        $clinic = $data['clinic'];
        $query = $this->db->where("(`clinic1` LIKE '%$clinic%' OR `clinic2` LIKE '%$clinic%' OR `clinic3` LIKE '%$clinic%' OR `clinic4` LIKE '%$clinic%' OR `clinic5` LIKE '%$clinic%')");
      }
      $query = $this->db->get($table);
    }

    if($query->num_rows())
    {
      return $query->result_array();
    }
    else
    {
      return null;
    }
  }

  function countSearchResults($data,$select = '')
  {
    $table = $data['table'];
    $query = $this->db->select($select);

    if($table == 'hospital')
    {
      // echo 'hello';
      $classification = $data['type'];
      $region = $data['region'];
      $province = $data['province'];
      $city = $data['city'];
      $search = $data['search'];
      $query = $this->db->where('classification',$classification);

      if(!empty($region))
      {
        // echo 'region';
        $query = $this->db->where('region',$region);
      }
      if(!empty($province))
      {
        // echo 'province';
        $query = $this->db->where('province',$province);
      }
      if(!empty($city))
      {
        // echo 'city';
        $query = $this->db->where('city',$city);
      }
      if(!empty($search))
      {
        // echo 'name';
        $query->like('name',$search);
      }
    }
    else if($table == 'dentistsanddoctors')
    {
      // var_dump($data);
      $specialization = $data['specialization'];
      $search = $data['search'];
      $doctor_status = $data['directory'];
      $type = $data['type'];

      $query = $this->db->where('doctor_status', $doctor_status);
      $query = $this->db->where('type', $type);

      if(!empty($specialization))
      {
        $query = $this->db->where('specialization',$specialization);
      }
      if(!empty($search))
      {
        // echo 'hello';
        // $query->like("firstname", $search);
        // $query->or_like("middlename", $search);
        // $query->or_like("lastname", $search);
        $query = $this->db->where("(`firstname` LIKE '%$search%' OR `middlename` LIKE '%$search%' OR `lastname` LIKE '%$search%')");
      }
      if(!empty($data['clinic']))
      {
        $clinic = $data['clinic'];
        $query = $this->db->where("(`clinic1` LIKE '%$clinic%' OR `clinic2` LIKE '%$clinic%' OR `clinic3` LIKE '%$clinic%' OR `clinic4` LIKE '%$clinic%' OR `clinic5` LIKE '%$clinic%')");
      }
    }
    return $query = $this->db->count_all_results($table);
  }

  function getJoinEstablishment($data,$select = '')
  {
      $search = $data['search'];
      $doctor_status = $data['directory'];
      $type = $data['type'];
      $search = $data['search'];

      $this->db->select('dentistsanddoctors.id, dentistsanddoctors.firstname, dentistsanddoctors.middlename,  dentistsanddoctors.lastname, dentistsanddoctors.specialization,dentistsanddoctors.contact_number,dentistsanddoctors.mobile_number,dentistsanddoctors.fax_number,dentistsanddoctors.email, establishments.name,establishments.address');
      $this->db->from('dentistsanddoctors');
      // $this->db->join('doctors_clinics', 'dentistsanddoctors.id = doctors_clinics.doctor_id', 'left');
      $this->db->join('establishments', 'establishments.doctor_id = dentistsanddoctors.id', 'left');
      $this->db->where('dentistsanddoctors.doctor_status','new');
      $this->db->where('dentistsanddoctors.type',$type);
      if(!empty($data['specialization']))
      {
        $this->db->where('dentistsanddoctors.specialization',$data['specialization']);
      }
      if(!empty($data['province']))
      {
        $this->db->where('establishments.state',$data['province']);
      }
      if(!empty($data['city']))
      {
        $this->db->where('establishments.city',$data['city']);
      }
      if(!empty($search))
      {
        // echo 'hello';
        // $query->like("firstname", $search);
        // $query->or_like("middlename", $search);
        // $query->or_like("lastname", $search);
        $query = $this->db->where("(`dentistsanddoctors`.`firstname` LIKE '%$search%' OR `dentistsanddoctors`.`middlename` LIKE '%$search%' OR `dentistsanddoctors`.`lastname` LIKE '%$search%')");
      }
      // $this->db->group_by('establishments.doctor_id');
      $this->db->order_by('dentistsanddoctors.id');
      $query = $this->db->get();
      // print_r($this->db);
      $result = array();

      if($query->num_rows())
      {
        foreach($query->result_array() as $row)
        {
          if (count($result) > 0 && $row['id'] == $result[count($result) - 1]['id']) {
            // var_dump($row);
            $last_row = count($result) - 1;
            $result[$last_row]['address'][] = $row['address'];
          } else {
            $arr = $row;
            $address = $row['address'];
            $arr['address'] = array($address);
            $result[] = $arr;
          }
        }

        return $result;
      }
      else
      {
        return null;
      }
  }
}
?>