<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Directories extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model('directory_model');
  }

  function index()
  {
    $this->load->view('directory_view');
  }

  function searchField()
  {
    // var_dump($_POST);
    if($_POST['table'] == 'dentistsanddoctors' || $_POST['table'] == 'establishments')
    {
      if(isset($_POST['status']) && $_POST['status'] == 'new')
      {
        // join dentistsanddoctors & establishment table here
        if($_POST['field'] == 'province')
        {
          $field = 'state';
          $_POST['field'] = 'state';
        }
        $result = $this->directory_model->getFieldResult($_POST['table'],$_POST['field'],'');
      }
      $result = $this->directory_model->getFieldResult($_POST['table'],$_POST['field'],$_POST['type']);
    }
    else
    {
      $result = $this->directory_model->getFieldResult($_POST['table'],$_POST['field'],$_POST['type']);
    }


    $data = array(); //Create array
    if($result)
    {
      foreach($result as $key => $value)
      {
        $data[] = array('label'=>$value[$_POST['field']], 'value'=>$value[$_POST['field']]);
      }
    }
    // var_dump($data);
    echo json_encode($data);
  }

  function searchFieldSub()
  {
    $table = $_POST['table'];
    $field = $_POST['field'];
    $value = rawurldecode($_POST['value']);
    $sub = $_POST['sub'];
    if($table == 'establishments')
    {
      if($field == 'province')
      {
        $field = 'state';
      }
    }

    $result = $this->directory_model->getFieldSubResult($table,$field,$value,$sub);

    if($result)
    {
      foreach ($result as $key => $value)
      {
        $data[] = array('label'=>$value[$sub], 'value'=>$value[$sub]);
      }
    }
    echo json_encode($data);
  }

  function ajaxSearch()
  {
    // var_dump($_POST);
    $data['count'] = '0';
    if($_POST['table'] == 'establishments')
    {
      $result = $this->directory_model->getJoinEstablishment($_POST);
      $data['count'] = count($result);
    }
    else
    {
      $result = $this->directory_model->getSearchResults($_POST);
      $result_count = $this->directory_model->countSearchResults($_POST);
      $data['count'] = $result_count;
    }

    switch ($_POST['type'])
    {
      case 'hospital':
        $data['search'] = 'Hospital(s)';
        break;

      case 'clinic':
        $data['search'] = 'Clinic(s)';
        break;

      case 'DMD':
        $data['search'] = 'Dentist(s)';
        break;

      case 'MD':
        $data['search'] = 'Doctor(s)';
        break;

      default:
        # code...
        break;
    }

    if($_POST['table'] == 'hospital')
    {
      $data['hospital'] = $result;
      // $this->load->view('results_hospital_view',$data); // HTML FORMAT
      echo json_encode($data); // JSON FORMAT
    }
    else if($_POST['table'] == 'dentistsanddoctors')
    {
      $data['dentistsanddoctors'] = $result;
      echo json_encode($data);
    }
    else if($_POST['table'] == 'establishments')
    {
      $data['dentistsanddoctors'] = $result;
      echo json_encode($data);
    }
  }
}
?>